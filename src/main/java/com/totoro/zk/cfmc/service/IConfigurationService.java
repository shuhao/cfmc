package com.totoro.zk.cfmc.service;

import java.io.File ;
import java.io.InputStream ;
import java.util.List ;

import com.totoro.zk.cfmc.model.ConfigurationItem ;

/**
 * 上传配置文件处理接口
 * @author 80002165 @date 2017年6月6日 下午4:02:20
 */
public interface IConfigurationService {
    
    boolean uploadConfFile(String path, File file) ;
    
    boolean uploadConfFile(String path, InputStream is) ;
    
    boolean uploadConfFile(String path, List<ConfigurationItem> items) ;
    
    boolean uploadConfFile(String path, byte[] data) ;
    
    
}
