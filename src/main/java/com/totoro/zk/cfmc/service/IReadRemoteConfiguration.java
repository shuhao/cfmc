package com.totoro.zk.cfmc.service;

import java.io.InputStream ;

/**
 * 读取配置中心文件service 
 * @author 80002165 @date 2017年6月7日 上午9:16:10
 */
public interface IReadRemoteConfiguration {
    /**
     * 读取zookeeper上的配置数据，返回原始的byte数组
     * @author 80002165 @date 2017年6月7日 上午9:17:42
     * @param path
     * @return
     */
    byte[] readConfiguration(String path) ;
    /**
     * 读取zookeeper上的配置数据，返回原始的byte数组转换的String
     * @author 80002165 @date 2017年6月7日 上午9:18:08
     * @param path
     * @return
     */
    String readConfigurationToStr(String path) ;
    /**
     * 读取zookeeper上的配置数据，返回原始的byte数组转换为输入流
     * @author 80002165 @date 2017年6月7日 上午9:18:21
     * @param path
     * @return
     */
    InputStream readConfigurationToIS(String path) ;
}
