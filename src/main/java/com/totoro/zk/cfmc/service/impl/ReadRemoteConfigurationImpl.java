package com.totoro.zk.cfmc.service.impl;

import java.io.InputStream ;
import java.io.UnsupportedEncodingException ;

import com.totoro.zk.cfmc.conf.ZKConf ;
import com.totoro.zk.cfmc.register.CuratorClient ;
import com.totoro.zk.cfmc.service.IReadRemoteConfiguration ;
import com.totoro.zk.cfmc.util.IOStreamUtils ;

/**
 * @author 80002165 @date 2017年6月7日 上午9:21:59
 */
public class ReadRemoteConfigurationImpl implements IReadRemoteConfiguration{
    
    /*
     * @see com.totoro.zk.cfmc.service.IReadRemoteConfiguration#readConfiguration(java.lang.String)
     */
    @Override
    public byte[] readConfiguration(String path) {
        if(path != null && !path.trim().equals("")){
            CuratorClient client = CuratorClient.init(ZKConf.address) ;
            return client.getNodeData(path);
        }
        return null ;
    }

    /*
     * @see com.totoro.zk.cfmc.service.IReadRemoteConfiguration#readConfigurationToStr(java.lang.String)
     */
    @Override
    public String readConfigurationToStr(String path) {
        byte[] data = readConfiguration(path) ;
        if(data != null){
            try {
                return new String(data, "UTF-8") ;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null ;
    }

    /*
     * @see com.totoro.zk.cfmc.service.IReadRemoteConfiguration#readConfigurationToIS(java.lang.String)
     */
    @Override
    public InputStream readConfigurationToIS(String path) {
        byte[] data = readConfiguration(path) ;
        if(data != null){
            return IOStreamUtils.toInputStream(data) ;
        }
        return null ;
    }
    
}
