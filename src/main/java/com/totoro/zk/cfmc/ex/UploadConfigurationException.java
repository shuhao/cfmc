package com.totoro.zk.cfmc.ex;

/**
 * 添加配置异常
 * @author 80002165 @date 2017年6月6日 下午4:15:59
 */
public class UploadConfigurationException extends RuntimeException{
    private static final long serialVersionUID = -7492279702979187298L ;
    
    public UploadConfigurationException(){
        super() ;
    }
    
    public UploadConfigurationException(String msg){
        super(msg) ;
    }
    
    public UploadConfigurationException(String msg, Throwable t){
        super(msg, t) ;
    }
}
