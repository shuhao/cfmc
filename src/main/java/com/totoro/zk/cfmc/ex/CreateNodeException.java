package com.totoro.zk.cfmc.ex;

/**
 * 创建节点异常
 * @author 80002165 @date 2017年6月6日 上午9:15:02
 */
public class CreateNodeException extends RuntimeException{
    private static final long serialVersionUID = 1979341357740577283L ;
    
    public CreateNodeException(){
        super() ;
    }
    
    public CreateNodeException(String msg){
        super(msg) ;
    }
}
