package com.totoro.zk.cfmc.ex;

/**
 * 客户端操作节点异常
 * @author 80002165 @date 2017年6月7日 上午9:27:33
 */
public class ClientException extends RuntimeException{
    private static final long serialVersionUID = 2106333492609748453L ;
    
    public ClientException(){
        super();
    }
    public ClientException(String msg){
        super(msg) ;
    }
    public ClientException(String msg, Throwable t){
        super(msg, t) ;
    }
    
}
