package com.totoro.zk.cfmc.model;

/**
 * 上传的配置项，暂时支持字符串类型数据
 * @author 80002165 @date 2017年6月6日 下午4:09:24
 */
public class ConfigurationItem {
    private String key ;
    private String value ;
    public String getKey() {
        return key ;
    }
    public void setKey(String key) {
        this.key = key ;
    }
    public String getValue() {
        return value ;
    }
    public void setValue(String value) {
        this.value = value ;
    }
    
}
