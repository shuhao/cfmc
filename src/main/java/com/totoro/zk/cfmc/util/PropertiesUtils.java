package com.totoro.zk.cfmc.util;

import java.io.IOException ;
import java.io.InputStream ;
import java.util.Properties ;

/**
 * @author 80002165 @date 2017年6月7日 上午9:18:44
 */
public class PropertiesUtils {
    public static Properties getProperties(InputStream is){
        if(is != null ){
            Properties properties = new Properties() ;
            try {
                properties.load(is);
                return properties ;
            } catch (IOException e) {
                e.printStackTrace();
            } finally{
                IOStreamUtils.closHelp(is) ; 
            }
            
        }
        return null ;
    }
}
