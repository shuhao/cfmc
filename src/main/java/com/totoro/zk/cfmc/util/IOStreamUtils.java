package com.totoro.zk.cfmc.util ;

import java.io.BufferedInputStream ;
import java.io.ByteArrayInputStream ;
import java.io.File ;
import java.io.FileInputStream ;
import java.io.FileNotFoundException ;
import java.io.FileOutputStream ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.OutputStream ;

/**
 * io流操作工具类
 * @author 80002165 @date 2017年6月6日 下午4:29:05
 */
public class IOStreamUtils {
    public static byte[] readInputStreamWithBuf(InputStream is) {
        int size = 100 ;
        BufferedInputStream bis = null ;
        try {
            bis = new BufferedInputStream(is) ;
            byte[] cache = new byte[size] ;
            byte[] data = new byte[bis.available()] ;
            int temp = 0 ;
            int len = 0 ;
            while ((temp = bis.read(cache)) != -1) {
                System.arraycopy(cache, 0, data, len, temp) ;
                len += temp ;
            }
            return data ;
        } catch (IOException e) {
            e.printStackTrace() ;
        } finally {
            closHelp(bis);
            closHelp(is);
        }
        return null ;
    }
    
    public static byte[] readInputStream(InputStream is) {
        try {
            int len = is.available() ;
            byte[] data = new byte[len] ;
            is.read(data) ;
            return data ;
        } catch (IOException e) {
            e.printStackTrace() ;
        } finally {
            closHelp(is);
        }
        return null ;
    }
    
    public static InputStream toInputStream(byte[] data) {
        InputStream is = null ;
        try {
            is = new ByteArrayInputStream(data) ;
        } catch (Exception e) {
            e.printStackTrace() ;
        }
        return is ;
    }
    
    public static void closHelp(InputStream is){
        if(is != null){
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void closHelp(OutputStream os){
        if(os != null){
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * 替换原来的配置文件
     * @author 80002165 @date 2017年6月7日 上午11:46:39
     * @param path 系统配置文件路径
     * @param fileName 配置文件名称
     * @param is 新文件流
     */
    public static void replaceConfFile(String path, String fileName, byte[] data){
        FileOutputStream fos = null ;
        try {
            File pathF = new File(path) ;
            if(!pathF.exists()){
                pathF.mkdirs() ;
            }
            File newConfFile = new File(path + File.separator + fileName) ;
            if(newConfFile.exists()){
                newConfFile.delete() ;
            }else{
                newConfFile.createNewFile() ;
            }
            fos = new FileOutputStream(newConfFile) ;
            fos.write(data); 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            closHelp(fos);
        }
    }
}
