package com.totoro.zk.cfmc.conf ;

/**
 * 状况客户端curator的配置项
 * @author 80002165 @date 2017年6月6日 上午8:57:26
 */
public class ZKClientConf {
    private int session_timeout = 10000 ;
    private int connect_timeout = 15000 ;
    
    public int getSession_timeout() {
        return session_timeout ;
    }
    
    public void setSession_timeout(int session_timeout) {
        this.session_timeout = session_timeout ;
    }
    
    public int getConnect_timeout() {
        return connect_timeout ;
    }
    
    public void setConnect_timeout(int connect_timeout) {
        this.connect_timeout = connect_timeout ;
    }
    
}
