package com.totoro.zk.cfmc.test;

import org.apache.curator.RetryPolicy ;
import org.apache.curator.framework.CuratorFramework ;
import org.apache.curator.framework.CuratorFrameworkFactory ;
import org.apache.curator.retry.RetryNTimes ;
import org.apache.curator.utils.EnsurePath ;
import org.apache.zookeeper.CreateMode ;

/**
 * @author 80002165 @date 2017年6月6日 下午5:32:00
 */
public class Test {
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        RetryPolicy retry = new RetryNTimes(1000, 3) ;
        CuratorFramework client = CuratorFrameworkFactory.builder().connectString("127.0.0.1:2181").connectionTimeoutMs(10000).sessionTimeoutMs(20000).retryPolicy(retry).build() ;
        client.start();
        
        try {
            byte[] data = client.getData().forPath("/cfmc/etrs/logback") ;
            System.out.println(new String(data, "UTF-8")) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
//        client.usingNamespace("cfmc") ;
//        
//        EnsurePath ensurePath = new EnsurePath("/spring") ;
//        try {
//            ensurePath.ensure(client.getZookeeperClient());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            String path = client.create().creatingParentContainersIfNeeded().withMode(CreateMode.EPHEMERAL).forPath("/cfmc/bams/logback/test") ;
//            System.out.println(path) ;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
