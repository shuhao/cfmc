package com.totoro.zk.cfmc;

import java.io.InputStream ;
import java.util.Properties ;

import com.totoro.zk.cfmc.conf.ZKConf ;
import com.totoro.zk.cfmc.service.impl.ReadRemoteConfigurationImpl ;
import com.totoro.zk.cfmc.util.IOStreamUtils ;
import com.totoro.zk.cfmc.util.PropertiesUtils ;

/**
 * @author 80002165 @date 2017年6月7日 上午9:21:02
 */
public class ClientTest {
    public static void main(String[] args) {
        ReadRemoteConfigurationImpl test = new ReadRemoteConfigurationImpl() ;
        String path = "/cfmc/etrs/properties/sys_variable.properties" ;
        ZKConf.address = "127.0.0.1:2181" ;
        InputStream is = test.readConfigurationToIS(path) ;
        Properties properties = PropertiesUtils.getProperties(is) ;
        String param = (String) properties.get("file.path") ;
        System.out.println(param) ;
        
        //复制文件
        byte[] data = test.readConfiguration(path) ;
        IOStreamUtils.replaceConfFile("D:\\etrs_sys", "sys_variable.properties", data);
    }
}
